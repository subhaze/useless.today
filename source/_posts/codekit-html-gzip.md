---
title: CodeKit + HTML gzip
desc: Random thoughts
tags:
  - CodeKit
  - CodeKit Hooks
  - gzip
date: 2016-10-29 15:00:00
---

**[UPDATE]** CodeKit 3 is now available and hooks work a bit differently now. This "should" still work, but, I'll try to have an update on this using the new hooks.

This is a quick write-up on how to gzip your HTML files with [CodeKit](http://incident57.com/codekit/index.html) Version 2.3+ hooks; Jade is used in the example but any of the HTML processors available should work the same.
<!-- more -->
First you'll need to setup a hook, the hook I've used is:

*   `Any` of the following are true
*   `The filename of any processed file` `ends with` `.jade`

Now you'll need to select `Shell Script (/bin/sh)` from the drop down and paste the follow into the textarea:

```bash

    # set CK_OUTPUT_PATHS to an empty string if it is not present
    ${CK_OUTPUT_PATHS:=""}

    # split string into an array via ':'
    CK_OUTPUT_PATHS=(${CK_OUTPUT_PATHS//:/ })

    for i in "${CK_OUTPUT_PATHS[@]}"  
    do  
        gzip -nc "$i" > "$i.gz"
    done  

```

The above code accesses a newly available environment variable that CodeKit is now setting `CK_OUTPUT_PATHS`. This variable is a `:` delimited string of all the files that were produced by the action defined in the hook above.

The line `gzip -nc $i > $i.gz` takes the current HTML file, gzips it, and saves a new version of it with the `.gz` extension. If you just want to replace the current HTML file with no `.gz` extension all you should have to do is use `gzip -nc $i` instead which will convert the current html files into a gzipped version.

With this hook setup, if you save a jade file that only effects one file then only one file will be gzipped and if you change a layout that's extended by many other files then all of those files will be gzipped.