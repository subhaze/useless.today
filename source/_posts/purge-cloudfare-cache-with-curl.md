title: Purging Cloudflare Cache With cURL
desc: Using cURL and Cloudflare's API to purge cache to a site.
tags: ["Cloudflare", "CLI"]
date: 2018-10-07

---

[Cloudflare](https://www.cloudflare.com/) is an amazing CDN provider as well as a great DNS service, and many more things nowdays; It also gives you free TLS for your websites! Because of this it's become my go-to for static sites like this one but, I dislike having to sign in to Cloudflare each time I make an update to my site. Thankfully they also have a nice API to work with and something we can utilize within our CI/CD pipelines to purge cache after we make updates.

<!-- more -->

There's a whole slew of information about their API [here](https://api.cloudflare.com). What we're interested in is the 'zone' portion of their API which allows for full site purges, by URL, and by cache-tags or host. I usually stick with the full site purge just because it's easier to toss everything out and allow it to naturally flow back into their cache without missing anything.

To do this you'll need your `zone id` of the domain, `api key` for your account, and an `email address` associated to the domain's account. You can find your `zone id` by going to the overview of the domain you're interested in, the `api key` can be found at the bottom of your account profile (using the top right drop-down), and the email is the one you use to sign in to your account. Once you have all that you can use the following [cURL](https://github.com/curl/curl) command to purge your cache!

```bash
curl -X POST "https://api.cloudflare.com/client/v4/zones/$CF_ZONE_ID/purge_cache" -H "X-Auth-Email:$CF_EMAIL" -H "X-Auth-Key:$CF_API_KEY" -H "Content-Type:application/json" --data '{"purge_everything":true}'
```

If it executed successfully you should see a response similar to `{"result":{"id":"xxxxxxxxxxxxx"},"success":true,"errors":[],"messages":[]}`.

And that's it!
