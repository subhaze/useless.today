---
title: Simple Ghost Blog Theme deploys with rsync
desc: Random thoughts
tags:
  - deploy
  - rsync
  - make
  - ghost-theme
date: 2015-04-26 13:51:10
---

So, I've finally jumped onto the Ghost Blog platform to give blogging a try. The first thing I wanted was an easy way to make updates to a theme and push those updates to my server and take care of the ghost restart.
<!-- more -->
This write-up assumes you're using Ubuntu and upstart to handle the Ghost restart, however, that line of code can be easily modified to use whatever you have setup to restart Ghost on your server. The hard requirements are rsync on your local machine and server, ability to run Make, and SSH access to your server.

> By **deploy** I mean, syncing what's on your local machine to a remote machine. So anything that's been updated gets pushed to the remote server and anything you've deleted on your local gets removed on the remote server.

For myself, the most basic way to deploy something is via [rsync](http://en.wikipedia.org/wiki/Rsync) which basically just keeps files and folders synced between two computers. I'm using a Makefile to easily trigger a few CLI commands by typing `make` in my terminal when I want to deploy. This also allows me to add more features pretty easily when I find the need to.

So, first off, let's look at what it takes to sync theme files from our local machine to our remote server which is the following one liner:

`rsync -a --delete ./ username@example.com:/var/www/ghost/content/themes/YOUR_THEME`

In the above we're asking rsync to run in '[archive move](http://serverfault.com/questions/141773/what-is-archive-mode-in-rsync)' and delete files on the remote that are not found on the local machine. You'll need to update `username` to the user you SSH in as, `example.com` to the domain of the server you're deploying to, and `/var/www/ghost/content/themes/YOUR_THEME` to the path of your theme.

So, we've got a nice one-liner for deploying files, but Ghost doesn't know that we've made these changes. SSH to the rescue, we can make another one-liner to restart the Ghost service on our remote server which will look something like this:

`ssh username@example.com service ghost restart`

Remember this is assuming your using something like upstart and have a service setup to start/stop/restart Ghost. If you're using a [Digital Ocean 1-Click Application](https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-ghost-application) you'll get this all setup for you.

Now that we have those two lines we can create a Makefile to trigger them for quick deploys. To do this, create a file called `Makefile` and save it along with your theme files, its contents should look something like this:

```bash Makefile

    deploy:  
        rsync -a --delete ./ username@example.com:/var/www/ghost/content/themes/YOUR_THEME
        ssh username@example.com service ghost restart

    .PHONY: deploy

```

With that file in place we can now quickly deploy via the terminal. Just fire up your terminal, `cd` into your theme directory you're wanting to deploy and then `make` or `make deploy` and your files will get synced up on your remote server and Ghost will be restarted.

If you're not familiar with Makefiles [sitepoint has a nice write-up](http://www.sitepoint.com/using-gnu-make-front-end-development-build-tool/) on using them in the front-end world.