---
title: CodeKit + babel
desc: Random thoughts
tags:
  - CodeKit
  - CodeKit Hooks
  - babeljs
date: 2016-10-29 15:00:00
---

**[UPDATE]** CodeKit 3 is now available and supports babel natively.

Currently [CodeKit](https://incident57.com/codekit/) does not support [babeljs](https://babeljs.io/) transpiling, [but appears it will at some point in the future](https://github.com/bdkjones/CodeKit/issues/554#issuecomment-116611607). This doesn't mean you can't use CodeKit to transpile babel, you'll just need to install babel locally and use a hook to do so.
<!-- more -->
There's one unfortunate thing with this hook, since we can't trigger a hook on file save _(unless you're running JSLint/JSHint, but the file name is not passed to the `CK_INPUT_PATHS` variable)_ we'll need to create specific output files for our babel files so that CodeKit will trigger hooks. This adds a minor bit of complexity, but doable with a small amount of work.

With this hook there are three variables of interest to you, `SRC`, `DIST`, and `SUFFIX`.

SRC: 

Is set to use a directory named `es6` so if you create your files in some other directory you'll want to change that.

DIST: 

Is where babel will save the transpiled files, currently that is set to `js`. Also, this folder **must** already exist, I may update this hook later so that it will create that folder if it does not already exist.

SUFFIX: 

This one is needed since we can't set CodeKit to trigger hooks on file save, hence adding a minor bit of complexity to this hook. So for each file you're wanting this hook to trigger on, you need to set the output path to `*-babel.js` _(or whatever you change this var to)_.

Also, make sure when you set the output file it's not saving the output in the root of the project but within the `es6` directory (or whatever directory you're using). 

Here's the hook criteria:

*   `Any` of the following are true
*   `the output path of any processed file` `ends with` `-babel.js`
*   Run the following `Shell Script (/bin/bash)`

Here's the hook code:

```bash

    # set CK_OUTPUT_PATHS to an empty string if it is not present
    ${CK_OUTPUT_PATHS:=""}

    PWD=`pwd` # path to directory CodeKit is watching  
    SRC="es6" # where our babel/es6 files will be  
    DIST="js" # where we want the transpiled files to go  
    SUFFIX="-babel.js" # the suffix we're using to trigger CodeKit Hooks

    # split string into an array via ':'
    CK_OUTPUT_PATHS=(${CK_OUTPUT_PATHS//:/ })

    for i in "${CK_OUTPUT_PATHS[@]}"  
    do  
        # We have are absolute file paths, what we want is relative paths
        relative_path="${i##$PWD/}"

        # We need to put together an output path for the file
        # So we prefix the the file with our $DIST folder after we've
        # remove the path to our working directory and our source directory
        output_path=$DIST/"${i##$PWD/$SRC/}"

        # now we're just running the babel CLI command against the relative
        # file path and telling it to save the file into our destination path
        # but, remove the '-bable.js' suffix work around we're using to trigger hooks
        babel "${relative_path}" --out-file "${output_path%$SUFFIX}".js

        # now we're just cleaning up the workaround file we must create in order to use
        # this hook within CodeKit
        rm "$i"
    done

```


##### Concat &amp; Minifying the Results

You could create a `main.js` file that's used to concat all your babel transpiled files and minify in one go.

The contents would look something like:

```js main.js

    // @codekit-append "path/to/js/file-one.js"
    // @codekit-append "path/to/js/file-two.js"
    // @codekit-append "path/to/js/file-three.js"

```

From there you can set an output file for `main.js` and adjust the `Output style:` to your liking.
<div class="tip">
I have ran into issues with the above concat/minify file not triggering, usually another save to my source file will trigger it.
</div>
Hopefully this will help you get up and running with babel and CodeKit or has inspired you to create your own hook with some other CLI tool.