---
title: CodeKit + timestamps
desc: Random thoughts
tags:
  - CodeKit
  - CodeKit Hooks
  - timestamps
date: 2016-10-29 15:00:00
---

**[UPDATE]** CodeKit 3 is now available and hooks work a bit differently now. This "should" still work, but, I'll try to have an update on this using the new hooks.

Here is another quick post on using the new Hook variables added in [CodeKit](http://incident57.com/codekit/index.html) 2.3, this time looking at adding timestamps to CSS files.
<!-- more -->
Even though this is focused on SCSS/CSS you should be able to take this example and modify it to work with any of the other preprocessing abilities  CodeKit has to offer. The main take away is, adding a unique placeholder for the timestamp in your source files and using [sed](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man1/sed.1.html) to replace it with a real timestamp when they're processed by CodeKit.

The hook settings used are:

*   `Any` of the following are true
*   `The filename of any processed file` `ends with` `.scss`

Then, we'll need to select `Shell Script (/bin/sh)` from the select box.

Once that's all set add the following to the textarea:

{% codeblock lang:bash %}

    # set CK_OUTPUT_PATHS to an empty string if it is not present
    ${CK_OUTPUT_PATHS:=""}

    # split string into an array via ':'
    CK_OUTPUT_PATHS=(${CK_OUTPUT_PATHS//:/ })

    now=$(date)

    for i in "${CK_OUTPUT_PATHS[@]}"  
    do  
        sed -i '' -e "s/{%raw%}{{TIMESTAMP}}{%endraw%}/${now}/" "$i"
    done  

{% endcodeblock %}

We're creating the timestamp with `now=$(date)` so if you wish to tweak the timestamp format you can [find more information here](http://www.cyberciti.biz/faq/linux-unix-formatting-dates-for-display/).

The line `sed -i -e "s/{%raw%}{{TIMESTAMP}}{%endraw%}/${now}/" "$i"` is using sed to execute a find/replace inline so that we overwrite the current file `$i` with the timestamp we created above. Right now the script is expecting to find `{%raw%}{{TIMESTAMP}}{%endraw%}` somewhere within the CSS file so that it can replace it.

So your SCSS file could look something like:

```scss site.scss

    /* {{TIMESTAMP}} */
    @import 'somthing';
    @import 'more';

    body{  
      ...
    }

```

and you will end up with a CSS file like:

```scss site.css

    /* Sun Apr 26 09:52:32 EDT 2015 */
    .something-css-here{
        ...
    }
    .more-css-here{
        ...
    }

    body{
        ...
    }

```